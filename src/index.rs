use crate::helpers::routes::*;
use crate::helpers::views::*;
use crate::partials::base::*;

use crate::post::Post;
use crate::user::User;

route_table! { @static: GET "/" => index }

pub async fn index(id: Identity) -> ApiResult {
    let conn = db::connection()?;
    Ok(Index {
        base: Base::with_identity(User::identify(id, &conn)),
        posts_users: Post::all_with_authors().load(&conn)?,
    }
    .render_ok())
}

pub struct Index {
    base: BaseIdentity,
    posts_users: Vec<(Post, User)>,
}

impl View for Index {
    fn render(self, context: &mut Context) -> &'static str {
        self.base.with_title("My Blog").render(context);
        context.insert("posts_users", &self.posts_users);

        "index.html"
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tables::examples::{posts, users};

    #[test]
    fn it_renders_index() {
        let user = users::user_one();
        let user2 = users::user_two();
        Index {
            base: Base::with_identified_user(user.clone()),
            posts_users: vec![
                (posts::post_one(user.id), user.clone()),
                (posts::post_two(user2.id), user2),
                (posts::post_three(user.id), user.clone()),
            ],
        }
        .render_ok();
    }
}
