use crate::index::url_for::index;
use crate::user::login_routes::url_for as login_routes;
use crate::{post, user};

resource_table![
    user => user::url_for [ .username ],
    post => post::url_for [ .title ],
    comment => post::url_for_comment,
];

action_table![
    home => index,
    login => login_routes::login,
    process_login => login_routes::process_login,
    logout => login_routes::logout,
    signup => login_routes::signup,
    process_signup => login_routes::process_signup,
];

#[cfg(test)]
pub mod examples {
    use super::*;
    pub use post::examples as posts;
    pub use user::examples as users;
}
