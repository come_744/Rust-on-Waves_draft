use crate::helpers::model::*;

use actix_identity::Identity;

#[derive(Debug, Clone, Queryable, Serialize, Identifiable)]
pub struct User {
    pub id: i32,
    pub username: String,
    pub email: String,
    // The password is hashed but it is better to be sure to not leak it.
    #[serde(skip_serializing)]
    pub password: String,
}

impl User {
    pub fn identify(id: Identity, conn: &DbConnection) -> Option<Self> {
        id.identity().and_then(|username| {
            let user = User::find_by_name(conn, &username).ok();
            if user.is_none() {
                id.forget();
            }
            user
        })
    }

    findable! {
        pub fn find_by_id(conn, id: i32) -> User => by_id;
        pub fn find_by_name(conn, name: &str) -> User => by_name;
    }

    pub fn by_id(id: i32) -> Filter<users::table, Eq<users::id, i32>> {
        users::table.filter(users::id.eq(id))
    }

    pub fn by_name(name: &str) -> Filter<users::table, Eq<users::username, &str>> {
        users::table.filter(users::username.eq(name))
    }

    #[allow(non_upper_case_globals)]
    pub const table: users::table = users::table;
}

#[derive(Debug, Insertable)]
#[table_name = "users"]
pub struct NewUser<'a> {
    pub username: &'a str,
    pub email: &'a str,
    pub password: String,
}

impl<'a> NewUser<'a> {
    pub fn insert(&self, conn: &DbConnection) -> Result<User, diesel::result::Error> {
        diesel::insert_into(users::table)
            .values(self)
            .get_result(conn)
    }
}

#[cfg(test)]
pub mod examples {
    use super::*;

    pub fn insertable() -> NewUser<'static> {
        NewUser {
            username: "some_user",
            email: "me@email.com",
            password: String::new(),
        }
    }

    pub fn user_one() -> User {
        User {
            id: 1,
            username: String::from("John"),
            email: String::from("john.doe@email.org"),
            password: String::from("abcdefg"),
        }
    }

    pub fn user_two() -> User {
        User {
            id: 2,
            username: String::from("Alice"),
            email: String::from("alice@company.com"),
            password: String::from("hijklm"),
        }
    }

    pub fn user_three() -> User {
        User {
            id: 3,
            username: String::from("Bob"),
            email: String::from("bob@company.com"),
            password: String::from("nop"),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_can_insert_a_new_user() {
        with_db(|conn| {
            examples::insertable().insert(conn).unwrap();
        })
    }
}
