use super::{model::*, views};
use crate::helpers::routes::*;

use crate::partials::Base;
use crate::post::{Comment, Post};

route_table! {
    GET "/users/{id}" => show(id),
@static:
    GET "/users" => list,
}

async fn list(id: Identity) -> ApiResult {
    let conn = db::connection()?;
    let base = Base::with_identity(User::identify(id, &conn));
    let users = User::table.load::<User>(&conn)?;

    Ok(views::List { base, users }.render_ok())
}

async fn show(path: web::Path<String>, id: Identity) -> ApiResult {
    let conn = db::connection()?;
    let user_id = path.into_inner();
    let user = match user_id.parse() {
        Ok(uid) => User::find_by_id(&conn, uid)?,
        Err(_) => User::find_by_name(&conn, &user_id)?,
    };

    Ok(views::Profile {
        base: Base::with_identity(User::identify(id, &conn)),
        posts: Post::belonging_to(&user).load(&conn)?,
        comments: Comment::belonging_to(&user).load(&conn)?,
        user,
    }
    .render_ok())
}

pub mod login_routes {
    use super::super::{model::*, views};
    use crate::helpers::routes::*;
    use actix_identity::Identity;
    use actix_web::Responder;

    route_table! {
    @static:
        GET "/login" => login,
        POST "/login" => process_login,
        GET "/logout" => logout,
        GET "/signup" => signup,
        POST "/signup" => process_signup,
    }

    async fn login(id: Identity) -> HttpResponse {
        if let Some(username) = id.identity() {
            HttpResponse::Ok().body(format!("Already logged in as {}", username))
        } else {
            views::Login.render_ok()
        }
    }

    #[derive(Deserialize)]
    pub struct LoginForm {
        pub username: String,
        password: String,
    }

    impl LoginForm {
        pub fn is_valid(&self, conn: &db::DbConnection) -> bool {
            if let Ok(user) = User::find_by_name(conn, &self.username) {
                argonautica::Verifier::default()
                    .with_hash(user.password)
                    .with_password(&self.password)
                    .with_secret_key(secret_key())
                    .verify()
                    .unwrap_or(false)
            } else {
                false
            }
        }
    }

    async fn process_login(login: web::Form<LoginForm>, id: Identity) -> ApiResult {
        let conn = db::connection()?;
        let username = &login.username;
        let response = if login.is_valid(&conn) {
            let session_token = String::from(username);
            id.remember(session_token);
            HttpResponse::Ok().body(format!("Logged as {}", username))
        } else {
            HttpResponse::Ok().body("Username or password is incorrect.")
        };
        Ok(response)
    }

    async fn logout(id: Identity) -> impl Responder {
        id.forget();
        HttpResponse::Ok().body("Logged out.")
    }

    async fn signup(id: Identity) -> impl Responder {
        if let Some(username) = id.identity() {
            HttpResponse::Ok().body(format!("Already logged in as {}", username))
        } else {
            views::Signup.render_ok()
        }
    }

    #[derive(Deserialize)]
    pub struct NewUserForm {
        pub username: String,
        pub email: String,
        password: String,
    }

    impl NewUserForm {
        pub fn hashed(&self) -> ApiResult<NewUser> {
            let hash = argonautica::Hasher::default()
                .with_password(&self.password)
                .with_secret_key(secret_key())
                .hash()?;

            Ok(NewUser {
                username: &self.username,
                email: &self.email,
                password: hash,
            })
        }
    }

    async fn process_signup(new_user: web::Form<NewUserForm>) -> ApiResult<String> {
        let conn = db::connection()?;
        new_user.hashed()?.insert(&conn)?;
        Ok(format!("Successfully saved user: {}", new_user.username))
    }

    fn secret_key() -> String {
        std::env::var("SECRET_KEY").expect("SECRET_KEY must be set")
    }
}
