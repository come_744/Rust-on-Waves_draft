use super::model::User;
use crate::helpers::views::*;
use crate::partials::base::*;

use crate::post::{Comment, Post};

pub struct Login;

impl View for Login {
    fn render(self, context: &mut Context) -> &'static str {
        Base::without_identity().with_title("Login").render(context);

        "user/login.html"
    }
}

pub struct List {
    pub base: BaseIdentity,
    pub users: Vec<User>,
}

impl View for List {
    fn render(self, context: &mut Context) -> &'static str {
        self.base.with_title("Users").render(context);
        context.insert("users", &self.users);

        "user/index.html"
    }
}

pub struct Profile {
    pub base: BaseIdentity,
    pub user: User,
    pub posts: Vec<Post>,
    pub comments: Vec<Comment>,
}

impl View for Profile {
    fn render(self, context: &mut Context) -> &'static str {
        let title = format!("{} - profile", self.user.username);
        self.base.with_title(&title).render(context);
        context.insert("user", &self.user);
        context.insert("posts", &self.posts);
        context.insert("comments", &self.comments);

        "user/show.html"
    }
}

pub struct Signup;

impl View for Signup {
    fn render(self, context: &mut Context) -> &'static str {
        Base::without_identity()
            .with_title("Sign Up")
            .render(context);

        "user/signup.html"
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tables::examples::{posts, users};

    #[test]
    fn it_renders_login() {
        Login.render_ok();
    }

    #[test]
    fn it_renders_list_0() {
        let base = Base::without_identity();
        let users = Vec::new();
        List { base, users }.render_ok();
    }

    #[test]
    fn it_renders_list_max() {
        let base = Base::without_identity();
        let users = vec![users::user_one(), users::user_two(), users::user_three()];
        List { base, users }.render_ok();
    }

    #[test]
    fn it_renders_profile_0() {
        Profile {
            base: Base::without_identity(),
            user: users::user_one(),
            posts: Vec::new(),
            comments: Vec::new(),
        }
        .render_ok();
    }

    #[test]
    fn it_renders_profile_max() {
        let user = users::user_one();
        let author2 = users::user_two();
        let author3 = users::user_three();
        let post2 = posts::post_two(author2.id);
        let post3 = posts::post_three(author3.id);
        let user_id = user.id;
        let comment1 = posts::comment_one(post2.id, user_id, None);
        let comment1_id = comment1.id;
        Profile {
            base: Base::without_identity(),
            user,
            posts: vec![
                posts::post_one(user_id),
                posts::post_two(user_id),
                posts::post_three(user_id),
            ],
            comments: vec![
                comment1,
                posts::comment_two(post3.id, user_id, Some(comment1_id)),
                posts::comment_three(post2.id, user_id, None),
            ],
        }
        .render_ok();
    }

    #[test]
    fn it_renders_signup() {
        Signup.render_ok();
    }
}
