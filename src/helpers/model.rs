pub use super::ApiResult;
pub use crate::schema::*;
pub use db::DbConnection;
pub use diesel::dsl::*;
pub use diesel::prelude::*;
pub use serde::Serialize;

use super::db;

#[cfg(test)]
pub fn with_db<F>(f: F) -> ()
where
    F: Fn(&db::DbConnection) -> (),
{
    dotenv::dotenv().unwrap();
    let conn = db::connection().unwrap();
    conn.test_transaction::<(), (), _>(|| {
        f(&conn);
        Ok(())
    });
}

#[macro_export]
macro_rules! findable {
    ($(
            pub fn $find_fn:ident ($(&$self:ident,)? conn $(, $($arg:ident: $type:ty),+)?)
            -> $what:ty
            => $fn:ident
      );* $(;)?) => {
        $(
            pub fn $find_fn($(&$self,)? conn: &DbConnection $(, $($arg: $type),+)?)
            -> ApiResult<$what> {
                Ok(Self::$fn($(&$self,)? $($($arg),+)?).first(conn)?)
            }
         )*
    }
}
