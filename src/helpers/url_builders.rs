/// Creates functions to map resources to URLs.
///
/// Defines `url_for` and `default_text_for_link_to`, used by Tera functons `url_for` and
/// `link_to`, defined in `src/helpers/views.rs`.
///
///
/// The syntax for the following, rerpeated once per resource:
///
/// ```text
/// <resource> => <function> [ \[ .<default_attr> \] ],
/// ```
///
/// - The resource is an identifier
/// - The function is path to a function to be called by `url_for` to build urls
/// - The optional default text attribute is a way for `default_text_for_link_` to guess the text
///   to put on the link. It is meant to be a `&'static str` which will be given as an index to a
///   `serde_json` object representing the resource.
///
/// # Examples
///
/// The example below:
/// - redirects Tera calls:
///     - `url_for(user=...)` to `user::url_for`,
///     - `url_for(post=...)` to `post::url_for`,
///     - `url_for(comment=...)` to `post::url_for_comment`,
/// - assigns a default text for Tera calls:
///     - `link_to(user=my_user)` to `my_user["usernanme"]` aka `my_user.username`
///     - `link_to(post=my_post)` to `my_post["title"]` aka `my_post.title`
///
/// ```rust
/// resource_table![
///     user => user::url_for [ .username ],
///     post => post::url_for [ .title ],
///     comment => post::url_for_comment,
/// ];
/// ```
#[macro_export]
macro_rules! resource_table {
    ($(
            $resource:ident => $func:path $( [ .$default_attr:ident ] )?
      ),+ $(,)?) => {
        pub fn url_for(
            key: &str,
            value: &serde_json::Value,
            action: Option<&str>,
        ) -> tera::Result<String> {
            match key {
                $( stringify!($resource) => $func (value, action), )+
                _ => None,
            }
            .ok_or_else(|| {
                tera::Error::from(match action {
                    Some(action) => format!("No url for ({}={}, action='{}')", key, value, action),
                    None => format!("No url for ({}={})", key, value),
                })
            })
        }

        pub fn default_text_for_link_to<'a>(kv: (&str, &'a serde_json::Value)) -> Option<&'a str> {
            let (key, value) = kv;
            if let serde_json::Value::Object(_value) = value {
                match key {
                    $($( stringify!($resource) => _value[stringify!($default_attr)].as_str(), )?)+
                    _ => None,
                }
            } else {
                None
            }
        }
    };
}

/// Creates functions to map actions to URLs.
///
/// Defines `url_for_action`, used by Tera function `url_for` defined in `src/helpers/views.rs`.
///
/// The syntax for the following, rerpeated once per resource:
///
/// ```text
/// <action> => <function>,
/// ```
///
/// - The action is an identifier
/// - The function is path to a function to be called by `url_for` to build urls
///
/// # Examples
///
/// The example below:
/// - redirects Tera calls:
///     - `url_for(action="home")` to `index`,
///     - `url_for(action="login")` to `login_routes::login`,
///     - `url_for(action="process_login")` to `login_routes::process_login`,
///     - `url_for(action="logout")` to `login_routes::logout`,
///
/// ```rust
/// action_table![
///     home => index,
///     login => login_routes::login,
///     process_login => login_routes::process_login,
///     logout => login_routes::logout,
/// ];
/// ```
#[macro_export]
macro_rules! action_table {
    ($(
            $resource:ident => $func:path
      ),+ $(,)?) => {
        pub fn url_for_action(action: &str) -> tera::Result<String> {
            match action {
                $( stringify!($resource) => Some($func()), )+
                _ => None,
            }
            .ok_or_else(|| format!("url_for(action='{}'): Unknown action", action).into())
        }
    };
}
