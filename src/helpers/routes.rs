pub use super::db;
pub use super::views::View;
pub use super::ApiResult;
pub use actix_identity::Identity;
pub use actix_web::{web, HttpResponse};
pub use diesel::{BelongingToDsl, RunQueryDsl};
pub use serde::Deserialize;
pub use serde_json::Value;

/// Creates functions to manage routes for a ressource.
///
/// Defines `init_routes(&mut web::ServiceConfig)` which initializes the routes, a module
/// `url_for` containing a function building the url for a route and a function `url_for` mapping
/// actions to the functions generated in `url_for` the module.
///
/// The syntax is the following, repeated once per route/action:
///
/// ```text
/// <method> <url_pattern> => <action> (<parameters>),
/// ```
///
/// If the method is specific to an instance of the resource, else:
///
/// ```text
/// <method> <url_pattern> => <action>,
/// ```
///
/// - The method can be:
///     - `GET`,
///     - `POST`,
///     - `PUT`,
///     - `DELETE`,
///     - `HEAD`,
///     - `OPTIONS`,
///     - `CONNECT`,
///     - `PATCH` or
///     - `TRACE`
/// - The action must be an existing controller function
/// - The parameters must match the url pattern `{}` tokens, they will be the parameters of the
///   generated url builder function, with type `impl Display`.
/// - The url pattern is the url assigned to the action
///
/// The action are split into two parts:
///
/// - The actions for one instance of the resource (e.g.: show, edit, delete)
/// - The actions which are global to the resources (e.g.: create, list)
///
/// # Examples
///
/// ```rust
/// route_table! {
///     GET "/posts/{id}" => show(id),
///     POST "/posts/{id}" => create_comment(id),
/// @static:
///     GET "/posts/submission" => new,
///     POST "/posts/submission" => create,
/// };
///
/// fn function_using_urls(post: Post) {
///     // The module `url_for` is defined by `route_tables!` invocation above.
///     let url_to_get_new_post_form = url_for::new();
///     let url_to_create_post = url_for::create();
///     let url_to_show_post = url_for::show(post.id);
///     let url_to_comment_post = url_for::comment(post.id);
/// }
///
/// fn main() {
///     // The function `init_routes` is defined by `route_table!` invocation above.
///     App::new()
///         .configure(init_routes)
/// }
///
/// // These functions are the controllers attached to the URLs and methods in the
/// // `route_tables!` after `init_routes` was called.
/// async fn index() -> impl Responder { unimplemented!() }
/// async fn create() -> impl Responder { unimplemented!() }
/// async fn show() -> impl Responder { unimplemented!() }
/// async fn create_comment() -> impl Responder { unimplemented!() }
/// ```
#[macro_export]
macro_rules! route_table {
    (
        pub fn $init_routes:ident,
        pub mod $url_for_mod:ident,
        pub fn $url_for_fn:ident,
    $(
        $method:ident $route:expr => $action:ident ($($arg:ident),* $(,)?)
    ),* $(,)?
    @static:
    $(
        $glob_method:ident $glob_route:expr => $glob_action:ident
    ),* $(,)? ) => {
        pub fn $init_routes(cfg: &mut web::ServiceConfig) {
            $(cfg.route(
                    $glob_route,
                    web::method(actix_web::http::Method::$glob_method).to($glob_action)
            );)*
            $(cfg.route($route, web::method(actix_web::http::Method::$method).to($action));)*
        }

        pub mod $url_for_mod {
            $(
            #[allow(unused)]
            pub fn $action($($arg: impl std::fmt::Display),*) -> String {
                format!($route $(, $arg = $arg)*)
            })*
            $(
            #[allow(unused)]
            pub fn $glob_action() -> String {
                String::from($glob_route)
            })*
        }

        #[allow(unused)]
        pub fn $url_for_fn(p: &Value, action: Option<&str>) -> Option<String> {
            match (p, action.unwrap_or("show")) {
                $( (Value::Object(p), stringify!($action)) => Some(
                        $url_for_mod::$action( $( &p[stringify!($arg)] ),* )
                ), )*
                (Value::String(a), _) if a.is_empty() => match action.unwrap_or("list") {
                    $(
                        stringify!($glob_action) => Some($url_for_mod::$glob_action()),
                    )*
                    _ => None,
                },
                _ => None,
            }
        }
    };
    ($(
        $method:ident $route:expr => $action:ident ($($arg:ident),* $(,)?)
    ),* $(,)?
    @static:
    $(
        $glob_method:ident $glob_route:expr => $glob_action:ident
    ),* $(,)? ) => {
        route_table! {
            pub fn init_routes,
            pub mod url_for,
            pub fn url_for,
            $(
                $method $route => $action ($($arg),*)
            ),*,
            @static:
            $(
                $glob_method $glob_route => $glob_action
            ),*
        }
    };
}
