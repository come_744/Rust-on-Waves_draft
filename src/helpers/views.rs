pub use crate::helpers::ApiResult;
pub use tera::Context;

use crate::tables::{default_text_for_link_to, url_for, url_for_action};
use actix_web::HttpResponse;
use actix_web::HttpResponseBuilder;
use lazy_static::lazy_static;
use serde_json::Value;
use std::collections::HashMap;
use tera::Tera;

lazy_static! {
    static ref TERA: Tera = {
        let templates_path = std::env::var("TEMPLATES_PATH")
            .unwrap_or_else(|_e| String::from("templates/**/*.html"));
        let mut tera = Tera::new(&templates_path).unwrap();
        tera.register_function("url_for", UrlFor);
        tera.register_function("link_to", LinkTo);
        tera
    };
}

pub trait View: Sized {
    fn render(self, context: &mut Context) -> &'static str;

    fn render_ok(self) -> HttpResponse {
        self.render_for(HttpResponse::Ok())
    }

    fn render_for(self, mut builder: HttpResponseBuilder) -> HttpResponse {
        let mut context = Context::new();
        let file = self.render(&mut context);
        builder.body(TERA.render(file, &context).unwrap())
    }
}

pub trait Partial: Sized {
    fn render(self, context: &mut Context);
}

pub struct UrlFor;

impl tera::Function for UrlFor {
    fn call(&self, args: &HashMap<String, Value>) -> tera::Result<Value> {
        #[derive(Default)]
        struct Args<'a> {
            target: Option<(&'a str, &'a Value)>,
            action: Option<&'a str>,
        }
        let mut parsed = Args::default();

        for (k, v) in args {
            if k == "action" {
                if parsed.action.is_some() {
                    return Err("url_for() got 'action=' twice".into());
                }
                let v = v
                    .as_str()
                    .ok_or("url_for(action=XXX): XXX must be a string")?;
                parsed.action = Some(v);
            } else {
                if parsed.target.is_some() {
                    return Err("url_for() got two non-action arguments".into());
                }
                parsed.target = Some((k, v));
            }
        }

        match (parsed.target, parsed.action) {
            (None, None) => Err("url_for() called without arguments".into()),
            (None, Some(action)) => url_for_action(action),
            (Some((k, v)), action) => url_for(k, v, action),
        }
        .map(Value::String)
    }

    fn is_safe(&self) -> bool {
        true
    }
}

pub struct LinkTo;

impl tera::Function for LinkTo {
    fn call(&self, args: &HashMap<String, Value>) -> tera::Result<Value> {
        #[derive(Default)]
        struct Args<'a> {
            text: Option<&'a str>,
            target: Option<(&'a str, &'a Value)>,
            action: Option<&'a str>,
        }
        let mut parsed = Args::default();

        for (k, v) in args {
            if k == "text" {
                if parsed.text.is_some() {
                    return Err("link_to() got 'text=' twice".into());
                }
                let a = v.as_str().ok_or("link_to(text=) expects a string")?;
                parsed.text = Some(a);
            } else if k == "action" {
                if parsed.action.is_some() {
                    return Err("link_to() got 'action=' twice".into());
                }
                let a = v.as_str().ok_or("link_to(action=) expects a string")?;
                parsed.action = Some(a);
            } else {
                if parsed.target.is_some() {
                    return Err("link_to() got two non-action arguments".into());
                }
                parsed.target = Some((k, v));
            }
        }

        let text = parsed
            .text
            .or_else(|| parsed.target.and_then(default_text_for_link_to))
            .ok_or_else(|| tera::Error::from("link_to() called without 'text='"))?;

        let url = match (parsed.target, parsed.action) {
            (None, None) => Err("link_to() called without a target".into()),
            (None, Some(action)) => url_for_action(action),
            (Some(("url", u)), None) => u
                .as_str()
                .map(str::to_owned)
                .ok_or_else(|| "link_to(url=) expects a string".into()),
            (Some(("url", _)), Some(_)) => Err("link_to(url=) does not expect 'action='".into()),
            (Some((k, v)), action) => url_for(k, v, action),
        }?;

        Ok(format!("<a href=\"{}\">{}</a>", url, tera::escape_html(text)).into())
    }

    fn is_safe(&self) -> bool {
        true
    }
}
