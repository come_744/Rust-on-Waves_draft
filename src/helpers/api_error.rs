use actix_web::http::StatusCode;
use actix_web::{HttpResponse, ResponseError};
use diesel::result::Error as DieselError;

pub type ApiResult<Output = HttpResponse> = Result<Output, ApiError>;

#[derive(Debug)]
pub struct ApiError {
    pub status_code: u16,
    pub message: String,
}

impl ApiError {
    pub fn new(status_code: u16, message: String) -> Self {
        Self {
            status_code,
            message,
        }
    }
}

impl std::fmt::Display for ApiError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.message.as_str())
    }
}

impl From<r2d2::Error> for ApiError {
    fn from(e: r2d2::Error) -> Self {
        ApiError::new(500, format!("Failed getting db connection: {e}"))
    }
}

impl From<DieselError> for ApiError {
    fn from(e: DieselError) -> Self {
        match e {
            DieselError::DatabaseError(_, e) => ApiError::new(500, e.message().to_string()),
            DieselError::NotFound => ApiError::new(404, "Record not found".to_string()),
            e => ApiError::new(500, format!("Diesel error: {}", e)),
        }
    }
}

impl From<argonautica::Error> for ApiError {
    fn from(e: argonautica::Error) -> Self {
        ApiError::new(500, format!("Argonautica error: {}", e))
    }
}

impl ResponseError for ApiError {
    fn error_response(&self) -> HttpResponse {
        let status_code =
            StatusCode::from_u16(self.status_code).unwrap_or(StatusCode::INTERNAL_SERVER_ERROR);

        let message = if status_code.as_u16() < 500 {
            format!("Error {}: {}", status_code.as_u16(), self.message)
        } else {
            log::error!("Api error: {}", self.message);
            format!("Error {}", status_code)
        };

        HttpResponse::build(status_code).body(message)
    }
}
