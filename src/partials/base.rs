use crate::helpers::views::*;

use crate::user::User;

pub struct Base<'a> {
    title: &'a str,
    connected_user: Option<User>,
}

impl<'a> Base<'a> {
    pub fn with_identity(id: Option<User>) -> BaseIdentity {
        BaseIdentity(id)
    }

    pub fn with_identified_user(user: User) -> BaseIdentity {
        BaseIdentity(Some(user))
    }

    pub fn without_identity() -> BaseIdentity {
        BaseIdentity(None)
    }
}

impl<'a> Partial for Base<'a> {
    fn render(self, context: &mut Context) {
        if let Some(user) = self.connected_user {
            context.insert("_base__connected_user", &user);
        }
        context.insert("_base__title", self.title);
    }
}

pub struct BaseIdentity(Option<User>);

impl BaseIdentity {
    pub fn with_title(self, title: &str) -> Base {
        Base {
            title,
            connected_user: self.0,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tables::examples::users;

    struct BaseView<'a>(Base<'a>);

    impl View for BaseView<'_> {
        fn render(self, context: &mut Context) -> &'static str {
            self.0.render(context);
            "_base.html"
        }
    }

    #[test]
    fn it_renders_base_0() {
        let base = Base {
            title: "",
            connected_user: None,
        };
        BaseView(base).render_ok();
    }

    #[test]
    fn it_renders_base_max() {
        let base = Base {
            title: "My blog",
            connected_user: Some(users::user_one()),
        };
        BaseView(base).render_ok();
    }
}
