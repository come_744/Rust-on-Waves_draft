use super::model::{Comment, Post};
use crate::helpers::views::*;
use crate::partials::base::*;

use crate::user::User;

pub struct Show {
    pub base: BaseIdentity,
    pub post: Post,
    pub author: User,
    pub comments_and_authors: Vec<(Comment, User)>,
}

impl View for Show {
    fn render(self, context: &mut Context) -> &'static str {
        let post = self.post;
        let title = format!("{} - My Blog", post.title);
        self.base.with_title(&title).render(context);
        context.insert("post", &post);
        context.insert("user", &self.author);
        context.insert("comments_authors", &self.comments_and_authors);

        "post/show.html"
    }
}

pub struct New {
    pub base: BaseIdentity,
}

impl View for New {
    fn render(self, context: &mut Context) -> &'static str {
        self.base.with_title("Login").render(context);

        "post/new.html"
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tables::examples::{posts, users};

    #[test]
    fn it_renders_show_0() {
        let author = users::user_one();
        let post = posts::post_one(author.id);
        Show {
            base: Base::without_identity(),
            post,
            author: author.clone(),
            comments_and_authors: Vec::new(),
        }
        .render_ok();
    }

    #[test]
    fn it_renders_show_max() {
        let author = users::user_one();
        let author2 = users::user_two();
        let author3 = users::user_three();
        let post = posts::post_one(author.id);
        let comment1 = posts::comment_one(post.id, author2.id, None);
        let comment2 = posts::comment_one(post.id, author.id, Some(comment1.id));
        let comment3 = posts::comment_one(post.id, author3.id, Some(comment2.id));
        Show {
            base: Base::without_identity(),
            post,
            author: author.clone(),
            comments_and_authors: vec![
                (comment1, author2),
                (comment2, author),
                (comment3, author3),
            ],
        }
        .render_ok();
    }

    #[test]
    fn it_renders_new() {
        let base = Base::without_identity();
        New { base }.render_ok();
    }
}
