use super::{model::*, views};
use crate::helpers::routes::*;

use crate::partials::Base;
use crate::user::User;

use diesel::{BelongingToDsl, QueryDsl};

route_table! {
    GET "/posts/{id}" => show(id),
    POST "/posts/{id}" => create_comment(id),
@static:
    GET "/posts/submission" => new,
    POST "/posts/submission" => create,
}

pub fn url_for_comment(c: &Value, action: Option<&str>) -> Option<String> {
    use url_for::*;
    match (c, action.unwrap_or("show")) {
        (Value::Object(c), "show") => Some(show(&c["post_id"])),
        _ => None,
    }
}

#[derive(Debug, Deserialize)]
pub struct NewPostForm {
    pub title: String,
    pub link: String,
}

impl NewPostForm {
    pub fn with_author_id(self, author_id: i32) -> NewPost {
        NewPost {
            title: self.title,
            link: Some(self.link),
            author: author_id,
        }
    }
}

async fn new(id: Identity) -> ApiResult {
    let conn = db::connection()?;
    let response = if let Some(user) = User::identify(id, &conn) {
        let base = Base::with_identified_user(user);
        views::New { base }.render_ok()
    } else {
        HttpResponse::Unauthorized().body("User not logged in")
    };
    Ok(response)
}

async fn create(submission: web::Form<NewPostForm>, id: Identity) -> ApiResult {
    let conn = db::connection()?;
    if let Some(user) = User::identify(id, &conn) {
        submission
            .into_inner()
            .with_author_id(user.id)
            .insert(&conn)?;
        Ok(HttpResponse::Ok().body("Submitted."))
    } else {
        Ok(HttpResponse::Unauthorized().body("User not logged in."))
    }
}

async fn show(path: web::Path<i32>, id: Identity) -> ApiResult {
    let conn = db::connection()?;
    let post_id = path.into_inner();
    let post = Post::find_by_id(&conn, post_id)?;
    Ok(views::Show {
        base: Base::with_identity(User::identify(id, &conn)),
        author: post.find_author(&conn)?,
        comments_and_authors: Comment::belonging_to(&post)
            .inner_join(User::table)
            .load(&conn)?,
        post,
    }
    .render_ok())
}

#[derive(Debug, Deserialize)]
pub struct NewCommentForm {
    pub comment: String,
    pub post_id: i32,
    pub parent_comment_id: Option<i32>,
}

impl NewCommentForm {
    pub fn with_author_id(self, author_id: i32) -> NewComment {
        NewComment {
            user_id: author_id,
            comment: self.comment,
            post_id: self.post_id,
            parent_comment_id: self.parent_comment_id,
        }
    }
}

async fn create_comment(new_comment_form: web::Form<NewCommentForm>, id: Identity) -> ApiResult {
    let conn = db::connection()?;
    if let Some(ref username) = id.identity() {
        let author = User::find_by_name(&conn, username)?;
        new_comment_form
            .into_inner()
            .with_author_id(author.id)
            .insert(&conn)?;
        Ok(HttpResponse::Ok().body("Commented."))
    } else {
        Ok(HttpResponse::Unauthorized().body("Not logged in."))
    }
}
