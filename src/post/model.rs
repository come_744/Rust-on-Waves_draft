use crate::helpers::model::*;

use crate::user::User;

#[derive(Debug, Clone, Serialize, Queryable, Identifiable, Associations)]
#[belongs_to(User, foreign_key = "author")]
pub struct Post {
    pub id: i32,
    pub title: String,
    pub link: Option<String>,
    pub author: i32,
    pub created_at: chrono::NaiveDateTime,
}

impl Post {
    findable! {
        pub fn find_by_id(conn, id: i32) -> Post => by_id;
        pub fn find_author(&self, conn) -> User => author;
    }

    pub fn all_with_authors() -> InnerJoin<posts::table, users::table> {
        posts::table.inner_join(users::table)
    }

    pub fn author(&self) -> Filter<users::table, Eq<users::id, i32>> {
        User::by_id(self.author)
    }

    pub fn by_id(id: i32) -> Filter<posts::table, Eq<posts::id, i32>> {
        posts::table.filter(posts::id.eq(id))
    }
}

pub struct NewPost {
    pub title: String,
    pub link: Option<String>,
    pub author: i32,
}

impl NewPost {
    pub fn insert(self, conn: &DbConnection) -> Result<Post, diesel::result::Error> {
        use self::posts::dsl::*;
        diesel::insert_into(posts)
            .values((
                title.eq(self.title),
                link.eq(self.link),
                author.eq(self.author),
                created_at.eq(now),
            ))
            .get_result(conn)
    }
}

#[derive(Debug, Clone, Serialize, Queryable, Identifiable, Associations)]
#[belongs_to(Post)]
#[belongs_to(User)]
pub struct Comment {
    pub id: i32,
    pub comment: String,
    pub post_id: i32,
    pub user_id: i32,
    pub parent_comment_id: Option<i32>,
    pub created_at: chrono::NaiveDateTime,
}

pub struct NewComment {
    pub comment: String,
    pub post_id: i32,
    pub parent_comment_id: Option<i32>,
    pub user_id: i32,
}

impl NewComment {
    pub fn insert(&self, conn: &DbConnection) -> Result<Comment, diesel::result::Error> {
        use self::comments::dsl::*;
        diesel::insert_into(comments)
            .values((
                comment.eq(&self.comment),
                post_id.eq(self.post_id),
                user_id.eq(self.user_id),
                parent_comment_id.eq(self.parent_comment_id),
                created_at.eq(now),
            ))
            .get_result(conn)
    }
}

#[cfg(test)]
pub mod examples {
    use super::*;

    pub fn insertable(author_id: i32) -> NewPost {
        NewPost {
            title: String::from("My title"),
            link: None,
            author: author_id,
        }
    }

    pub fn post_one(author_id: i32) -> Post {
        Post {
            id: 1,
            title: String::from("My post"),
            link: None,
            author: author_id,
            created_at: chrono::NaiveDateTime::from_timestamp(0, 0),
        }
    }

    pub fn post_two(author_id: i32) -> Post {
        Post {
            id: 2,
            title: String::from("My second"),
            link: Some(String::from("https://example.com")),
            author: author_id,
            created_at: chrono::NaiveDateTime::from_timestamp(0, 0),
        }
    }

    pub fn post_three(author_id: i32) -> Post {
        Post {
            id: 3,
            title: String::from("Another post"),
            link: None,
            author: author_id,
            created_at: chrono::NaiveDateTime::from_timestamp(0, 0),
        }
    }

    pub fn insertable_comment(
        post_id: i32,
        author_id: i32,
        parent_comment_id: Option<i32>,
    ) -> NewComment {
        NewComment {
            comment: String::from("A new comment"),
            post_id,
            parent_comment_id,
            user_id: author_id,
        }
    }

    pub fn comment_one(post_id: i32, author_id: i32, parent_comment_id: Option<i32>) -> Comment {
        Comment {
            id: 1,
            comment: String::from("My comment"),
            post_id,
            user_id: author_id,
            parent_comment_id,
            created_at: chrono::NaiveDateTime::from_timestamp(0, 0),
        }
    }

    pub fn comment_two(post_id: i32, author_id: i32, parent_comment_id: Option<i32>) -> Comment {
        Comment {
            id: 2,
            comment: String::from("Second comment"),
            post_id,
            user_id: author_id,
            parent_comment_id,
            created_at: chrono::NaiveDateTime::from_timestamp(0, 0),
        }
    }

    pub fn comment_three(post_id: i32, author_id: i32, parent_comment_id: Option<i32>) -> Comment {
        Comment {
            id: 2,
            comment: String::from("Another comment"),
            post_id,
            user_id: author_id,
            parent_comment_id,
            created_at: chrono::NaiveDateTime::from_timestamp(0, 0),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tables::examples::{posts, users};

    #[test]
    fn it_can_insert_a_post() {
        with_db(|conn| {
            let author_id = users::insertable().insert(conn).unwrap().id;
            posts::insertable(author_id).insert(conn).unwrap();
        });
    }

    #[test]
    fn it_can_insert_a_comment() {
        with_db(|conn| {
            let author_id = users::insertable().insert(conn).unwrap().id;
            let post_id = posts::insertable(author_id).insert(conn).unwrap().id;
            posts::insertable_comment(post_id, author_id, None)
                .insert(conn)
                .unwrap();
        })
    }

    #[test]
    fn it_can_insert_a_child_comment() {
        with_db(|conn| {
            let author_id = users::insertable().insert(conn).unwrap().id;
            let post_id = posts::insertable(author_id).insert(conn).unwrap().id;
            let comment_id = posts::insertable_comment(post_id, author_id, None)
                .insert(conn)
                .unwrap()
                .id;
            posts::insertable_comment(post_id, author_id, Some(comment_id))
                .insert(conn)
                .unwrap();
        })
    }
}
