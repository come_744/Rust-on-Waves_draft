CREATE TABLE comments (
        id SERIAL PRIMARY KEY,
        comment VARCHAR NOT NULL,
        post_id INT NOT NULL,
        user_id INT NOT NULL,
        parent_comment_id INT,
        created_at TIMESTAMP NOT NULL,

        CONSTRAINT fk_post FOREIGN KEY(post_id) REFERENCES posts(id),
        CONSTRAINT fk_user FOREIGN KEY(user_id) REFERENCES users(id),
        CONSTRAINT fk_parent_comment FOREIGN KEY(parent_comment_id) REFERENCES comments(id)
);
